# Import module for data manipulation
import pandas as pd
# Import module for linear algebra
import numpy as np
# Import module for Fuzzy string matching
from fuzzywuzzy import fuzz, process
# Import module for regex
import re
# Import module for iteration
import itertools
# Import module for function development
from typing import Union, List, Tuple
# Import module for TF-IDF
from sklearn.feature_extraction.text import TfidfVectorizer
# Import module for cosine similarity
from sklearn.metrics.pairwise import cosine_similarity
# Import module for KNN
from sklearn.neighbors import NearestNeighbors
import mysqlx
import json
import pytz
from datetime import datetime, date, timedelta
import os
from dotenv import load_dotenv
import traceback, requests, socket
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}
# String pre-processing
# print(DB_PASS)
# print(DB_USER)
# print(DB_PORT)
# print(DB_HOST)


def matcher(sourceType, source, query, to_match, stringCleaner):
    def preprocess_string(s):
        # Remove spaces between strings with one or two letters
        s = re.sub(r'(?<=\b\w)\s*[ &]\s*(?=\w\b)', '', s)
        return s

    # String matching - TF-IDF

    def build_vectorizer(
        clean: pd.Series,
        analyzer: str = 'char',
        ngram_range: Tuple[int, int] = (1, 4),
        n_neighbors: int = 1,
        **kwargs
    ) -> Tuple:
        # Create vectorizer
        vectorizer = TfidfVectorizer(
            analyzer=analyzer, ngram_range=ngram_range, **kwargs)
        X = vectorizer.fit_transform(clean.values.astype('U'))

        # Fit nearest neighbors corpus
        nbrs = NearestNeighbors(n_neighbors=n_neighbors,
                                metric='cosine').fit(X)
        return vectorizer, nbrs

    # String matching - KNN

    def tfidf_nn(
        messy,
        clean,
        n_neighbors=1,
        **kwargs
    ):
        # Fit clean data and transform messy data
        vectorizer, nbrs = build_vectorizer(
            clean, n_neighbors=n_neighbors, **kwargs)
        input_vec = vectorizer.transform(messy)

        # Determine best possible matches
        distances, indices = nbrs.kneighbors(
            input_vec, n_neighbors=n_neighbors)
        nearest_values = np.array(clean)[indices]
        return nearest_values, distances, indices

    # String matching - match fuzzy

    def find_matches_fuzzy(
        row,
        match_candidates,
        _id,
        limit=1

    ):
        row_matches = process.extract(
            row, dict(enumerate(match_candidates)),
            scorer=fuzz.token_sort_ratio,
            limit=limit
        )
        result = [(row, match[0], match[1], _id) for match in row_matches]
        # print(result)
        return result

    # String matching - TF-IDF

    def fuzzy_nn_match(
            messy,
            clean,
            column,
            col,
            _ids,
            n_neighbors=100,
            limit=1, **kwargs):
        nearest_values, _, indices = tfidf_nn(
            messy, clean, n_neighbors, **kwargs)
        # print(nearest_values)
        results = [find_matches_fuzzy(row, nearest_values[i],  _ids[i], limit)
                   for i, row in enumerate(messy)]
        # print("r")
        df = pd.DataFrame(itertools.chain.from_iterable(results),
                          columns=[column, col, 'Ratio', "_id"]
                          )
        return df, indices

    # String matching - Fuzzy

    def fuzzy_tf_idf(
        df: pd.DataFrame,
        column: str,
        clean: pd.Series,
        mapping_df: pd.DataFrame,
        col: str,
        analyzer: str = 'char',
        ngram_range: Tuple[int, int] = (1, 3)
    ) -> pd.Series:
        # Create vectorizer
        clean = clean.drop_duplicates().reset_index(drop=True)
        messy_prep = df[column].dropna().reset_index(drop=True).astype(str)
        messy = messy_prep.apply(stringCleaner)
        result = fuzzy_nn_match(messy=messy, clean=clean,
                                column=column, col=col, n_neighbors=1, _ids=mapping_df['_id'])
        # Map value from messy to clean
        return result
    # print(query)
    # connect to db
    # Connect to server on localhost
    my_session = mysqlx.get_session({
        'host': DB_HOST, 'port': DB_PORT,
        'user': DB_USER, 'password': DB_PASS
    })

    try:

        my_schema = my_session.get_schema('in4freedom')

        # Use the collection 'my_collection'
        if sourceType == "collection":
            my_source = my_schema.get_collection(source)

            # print(my_source.count())

            # Specify which document to find with Collection.find() and
            # fetch it from the database with .execute()
            docs = my_source.find(query).execute()
        elif sourceType == "table":
            my_source = my_schema.get_table(source)
            docs = my_source.select().where(query).execute()
        # Print document
        docs = docs.fetch_all()

        my_session.close()

        # turn team columns into dfs
        converted_docs = []
        for doc in docs:
            converted_docs.append(json.loads(doc.as_str()))

        main_df = pd.DataFrame.from_dict(converted_docs, orient="columns")
        # ##print(main_df)
        try:
            home_teams = main_df[['_id', 'homeTeam']].copy()
        except KeyError:
            return False

        away_teams = main_df[['_id', 'awayTeam']].copy()
        if "baseball" in query.lower():
            if to_match['homeTeam'].lower() == "ny yankees".lower():
                to_match['homeTeam'] = "new york yankees"
            if to_match['awayTeam'].lower() == "ny yankees":
                to_match['awayTeam'] = "new york yankees"
            if to_match['homeTeam'].lower() == "chi cubs":
                to_match['homeTeam'] = "chicago cubs"
            if to_match['awayTeam'].lower() == "chi cubs":
                to_match['awayTeam'] = "chicago cubs"
            if to_match['homeTeam'].lower() == "tex rangers":
                to_match['homeTeam'] = "texas rangers"
            if to_match['awayTeam'].lower() == "tex rangers":
                to_match['awayTeam'] = "texas rangers"

            if to_match['homeTeam'].lower() == "was nationals":
                to_match['homeTeam'] = "washington nationals"
            if to_match['awayTeam'].lower() == "was nationals":
                to_match['awayTeam'] = "washington nationals"

            if to_match['homeTeam'].lower() == "Mil brewers":
                to_match['homeTeam'] = "Milwaukee brewers"
            if to_match['awayTeam'].lower() == "Mil brewers":
                to_match['awayTeam'] = "Milwaukee brewers"

            if to_match['homeTeam'].lower() == "cin reds":
                to_match['homeTeam'] = "cincinnati reds"
            if to_match['awayTeam'].lower() == "cin reds":
                to_match['awayTeam'] = "cincinnati reds"

            if to_match['homeTeam'].lower() == "tb rays":
                to_match['homeTeam'] = "tampa bay rays"
            if to_match['awayTeam'].lower() == "tb rays":
                to_match['awayTeam'] = "tampa bay rays"

            if to_match['homeTeam'].lower() == "cle guardians":
                to_match['homeTeam'] = "cleveland guardians"
            if to_match['awayTeam'].lower() == "cle guardians":
                to_match['awayTeam'] = "cleveland guardians"

            if to_match['homeTeam'].lower() == "kc royals":
                to_match['homeTeam'] = "kansas city royals"
            if to_match['awayTeam'].lower() == "kc royals":
                to_match['awayTeam'] = "kansas city royals"

            if to_match['homeTeam'].lower() == "chi white sox":
                to_match['homeTeam'] = "chicago white sox"
            if to_match['awayTeam'].lower() == "chi white sox":
                to_match['awayTeam'] = "chicago white sox"

            if to_match['homeTeam'].lower() == "stl cardinals":
                to_match['homeTeam'] = "st.louis cardinals"
            if to_match['awayTeam'].lower() == "stl cardinals":
                to_match['awayTeam'] = "st.louis cardinals"

            if to_match['homeTeam'].lower() == "ari diamondbacks":
                to_match['homeTeam'] = "arizona diamondbacks"
            if to_match['awayTeam'].lower() == "ari diamondbacks":
                to_match['awayTeam'] = "arizona diamondbacks"

            if to_match['homeTeam'].lower() == "pit pirates":
                to_match['homeTeam'] = "pittsburgh pirates"
            if to_match['awayTeam'].lower() == "pit pirates":
                to_match['awayTeam'] = "pittsburgh pirates"

            if to_match['homeTeam'].lower() == "sf giants":
                to_match['homeTeam'] = "san francisco giants"
            if to_match['awayTeam'].lower() == "sf giants":
                to_match['awayTeam'] = "san francisco giants"

            if to_match['homeTeam'].lower() == "col rockies":
                to_match['homeTeam'] = "colorado rockies"
            if to_match['awayTeam'].lower() == "col rockies":
                to_match['awayTeam'] = "colorado rockies"

            if to_match['homeTeam'].lower() == "mia marlins":
                to_match['homeTeam'] = "miami marlins"
            if to_match['awayTeam'].lower() == "mia marlins":
                to_match['awayTeam'] = "miami marlins"

        home_teams["toMatch"] = to_match['homeTeam']
        away_teams["toMatch"] = to_match['awayTeam']

        # print(home_teams)
        # print(away_teams)
        # create dataset
        # print("finding")
        home_df_result, home_indices = (home_teams.pipe(fuzzy_tf_idf,  # Function and messy data
                                                        column='toMatch',  # Messy column in data

                                                        # Master data (list)
                                                        clean=home_teams['homeTeam'],
                                                        mapping_df=home_teams,  # Master data
                                                        col='Result')  # Can be customized
                                        )
        # View the result of fuzzy string matching
        # print("found")
        # print(home_df_result)
        away_df_result, away_indices = (away_teams.pipe(fuzzy_tf_idf,  # Function and messy data
                                                        column='toMatch',  # Messy column in data

                                                        # Master data (list)
                                                        clean=away_teams['awayTeam'],
                                                        mapping_df=away_teams,  # Master data
                                                        col='Result')  # Can be customized
                                        )
        # View the result of fuzzy string matching
        # print("found")
        # print(away_df_result)
        # print(home_indices)
        # print(away_indices)

        # print(main_df.iloc[[home_indices[0][0]]])
        # print(main_df.iloc[[away_indices[0][0]]])

        if home_indices[0][0] == away_indices[0][0]:
            # print(main_df.iloc[[home_indices[0][0]]])
            return main_df.iloc[[home_indices[0][0]]]
        else:
            # print(main_df['homeTeam'])
            games = main_df[(main_df['homeTeam'] == home_df_result.iloc[0]['Result']) & (
                main_df['awayTeam'] == away_df_result.iloc[0]['Result'])]
            # print(games)
            try:
                return games.iloc[[0]]
            except IndexError:
                return False
    except:
        hostname = socket.gethostname()
        ## getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\","\\\\")
        err = traceback.format_exc().replace("\\","\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Matching String",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """


        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})



'''
test_data = {"id": "1888365", "host_id": "18671", "guest_id": "18828", "league_id": "602", "Round": "7", "Host_SC": None, "Guest_SC": None, "DATE": "2023-06-29", "DATE_BAH": "2023-06-29 01:00:00", "Pred_1": "80", "Pred_X": "13", "Pred_2": "7", "Host_SC_HT": None, "Guest_SC_HT": None, "kelly": None, "comment": None, "match_preview": None, "btr_link": None, "host_stadium": "Est\u00e1dio Waldomiro Borges", "match_stadium": None, "HOST_NAME": "Jequi\u00e9", "GUEST_NAME": "Leonico", "penalty_score": None, "extra_time_score": None, "loddname": None,
             "goalsavg": "1.5", "host_sc_pr": "1", "guest_sc_pr": "0", "weather_low": "19", "weather_high": "19", "weather_code": "25", "best_odd": None, "best_odd_frac": None, "best_odd_1": None, "best_odd_X": None, "best_odd_2": None, "odd_1_frac": None, "odd_X_frac": None, "odd_2_frac": None, "move_1": None, "move_X": None, "move_2": None, "short_tag": "BrB", "fxgid": None, "fxg2id": None, "spmjid": "18804077", "hasCommXml": None, "pick_day": "0", "isCup": "0", "is_international_club_cup": "0", "is_nationalteam_cup": "0", "code": "br", "fctr": "0"}
naive = datetime.now()
timezone = pytz.timezone("Europe/London")
aware1 = timezone.localize(naive)
##print(int(datetime.strptime(
    test_data['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp()) - int(aware1.utcoffset().total_seconds()))
##print(int(aware1.utcoffset().total_seconds()))
# f"eventDate > {int(datetime.strptime(test_data['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp())  - int(aware1.utcoffset().total_seconds())} AND eventDate < {int(datetime.strptime(test_data['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp())  - int(aware1.utcoffset().total_seconds())} AND sport = 'Football'"
matcher("collection", 'sports_events',
        f"eventDate > {int(datetime.strptime(test_data['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp()) - int(aware1.utcoffset().total_seconds()) - 3600} AND eventDate < {int(datetime.strptime(test_data['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp()) - int(aware1.utcoffset().total_seconds()) + 3600} AND sport = 'Football'", {'homeTeam': test_data['HOST_NAME'], 'awayTeam': test_data['GUEST_NAME']})
'''
